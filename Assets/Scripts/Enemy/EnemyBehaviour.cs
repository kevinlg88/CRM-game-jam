﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {
	

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D other){
		
		if(other.gameObject.CompareTag("Player")){
			if (other.contacts [0].normal.y < 0) {
				other.gameObject.GetComponent<Rigidbody2D> ().AddForce (Vector2.up * 100);
			}
		}
		if (other.gameObject.CompareTag ("FireBall")) {
			Destroy (this.gameObject);
		}
	}
}
