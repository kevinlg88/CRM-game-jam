﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMonkey1 : MonoBehaviour {

	[SerializeField]
	private float speed;
	//private Rigidbody2D rb2d;
	[SerializeField]
	private Transform patrol;
	private Vector3 posIni;
	private Vector3 target;
	private bool onRight = true;

	// Use this for initialization
	void Start () {
		posIni = transform.position;
		target = patrol.position;
		//rb2d = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		EnemyMoviment ();
	}

	void EnemyMoviment(){
		Vector2 direction = target - transform.position;
		transform.Translate (new Vector2(direction.normalized.x, 0) * speed * Time.fixedDeltaTime);
		if (Mathf.Abs (target.x - transform.position.x) < 0.1f) {
			if (target == posIni) {
				target = patrol.position;
			} else {
				target = posIni;
			}
		}

		if (direction.x < 0) {
			onRight = false;
		} else if (direction.x > 0) {
			onRight = true;
		}
	}
}
