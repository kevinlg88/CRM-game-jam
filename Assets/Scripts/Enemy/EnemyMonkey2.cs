﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMonkey2 : MonoBehaviour {

	public GameObject enemyTarget;

	private bool onRight = true;

	//Throw the object
	[Header("Time to fire again")]
	public float fireRate;
	private float canFire = 0;
	public float force;
	public GameObject objectThrow;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		EnemyMoviment ();
		EnemyAttack ();

	}

	void EnemyAttack(){
		if (enemyTarget != null) {
			if (Vector2.Distance (enemyTarget.transform.position, transform.position) < 7.5f) {
				if (onRight) {
					Vector2 direction = (enemyTarget.transform.position - new Vector3 (transform.position.x + 0.40f, transform.position.y, transform.position.z));
					direction.Normalize ();
					Debug.DrawLine (new Vector3 (transform.position.x + 0.40f, transform.position.y, transform.position.z), (new Vector3 (transform.position.x + 0.40f, transform.position.y, transform.position.z) + new Vector3 (direction.x, direction.y, 0)), Color.red);

					if (Time.time > canFire) {
						GameObject auxObject = Instantiate (objectThrow, new Vector2 (transform.position.x + 0.40f, transform.position.y), Quaternion.identity);
						auxObject.gameObject.SetActive (true);
						auxObject.GetComponent<Rigidbody2D> ().AddForce (direction * force);
						canFire = Time.time + fireRate;
					}
				} else {
					Vector2 direction = (enemyTarget.transform.position - new Vector3 (transform.position.x - 0.40f, transform.position.y, transform.position.z));
					direction.Normalize ();
					Debug.DrawLine (new Vector3 (transform.position.x - 0.40f, transform.position.y, transform.position.z), (new Vector3 (transform.position.x - 0.40f, transform.position.y, transform.position.z) + new Vector3 (direction.x, direction.y, 0)), Color.red);
				
					if (Time.time > canFire) {
						GameObject auxObject = Instantiate (objectThrow, new Vector2 (transform.position.x - 0.40f, transform.position.y), Quaternion.identity);
						auxObject.gameObject.SetActive (true);
						auxObject.GetComponent<Rigidbody2D> ().AddForce (direction * force);
						canFire = Time.time + fireRate;
					}
				}
			}
		}
	}

	void EnemyMoviment(){
		if (enemyTarget != null) {
			if (enemyTarget.transform.position.x < (transform.position.x - 0.40f)) {
				onRight = false;
			} else if (enemyTarget.transform.position.x > (transform.position.x + 0.40f)) {
				onRight = true;
			} else {
				canFire = Time.time + 1;
			}
		}
	}
}
