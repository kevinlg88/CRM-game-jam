﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	//Player public variables
	[Header("Player attributes")]
	public float speed;
	public float jumpStrength;

	[Header("Player Fire")]
	public GameObject FireBall;

	//Player state variables
	[HideInInspector]
	public bool canMove = true;
	private bool hasPower = false;
	private bool onRight = true;
	private bool onGround = false;
	private bool isRunning = false;
	[HideInInspector]
	public bool isDead = false;
	private bool isGolden = false;


	private Rigidbody2D rb;
	private Animator anim;
	private SpriteRenderer spriteRender;
	private float EixoH, EixoV; //Inputs



	[HideInInspector]
	public float offsetMinX;

	void Awake()
	{
		rb = this.GetComponent<Rigidbody2D> ();
		anim = transform.Find("SamBonzinho").GetComponent<Animator> ();
		spriteRender = transform.Find ("SamBonzinho").GetComponent<SpriteRenderer> ();
	}

	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (canMove) {
			PlayerMoviment ();
		}

		Fire ();
	}

	void PlayerMoviment(){

		EixoH = Input.GetAxisRaw ("Horizontal");
		EixoV = Input.GetAxisRaw ("Vertical");
		rb.velocity = new Vector2(EixoH * speed * Time.fixedDeltaTime, rb.velocity.y);
		if (EixoH < 0) {
			onRight = false;
			spriteRender.flipX = true;
			isRunning = true;
			anim.SetBool ("isRunning", true);
		} else if (EixoH > 0){
			onRight = true;
			spriteRender.flipX = false;
			isRunning = true;
			anim.SetBool ("isRunning", true);
		}

		if (rb.velocity.x == 0) {
			isRunning = false;
			anim.SetBool ("isRunning", false);
		}
		


		if (Input.GetButtonDown ("Jump"))
		{	
			if (onGround) {
				anim.SetBool("Jump",true);
				rb.velocity = Vector2.up * jumpStrength * Time.fixedDeltaTime;
			}
		}

		if (Input.GetKeyDown (KeyCode.V)) {

			if (isGolden) {
				ChangeToNormal ();
				isGolden = false;
			} else {
				ChangeToGolden ();
				isGolden = true;
			}
		}

		if (transform.position.x < offsetMinX) {
			transform.position = new Vector2 (offsetMinX, transform.position.y);
		}

	}

	private void ChangeToGolden()
	{

			transform.Find ("SamBonzinho").gameObject.SetActive (false);
			transform.Find ("SamGolden").gameObject.SetActive (true);
			anim = transform.Find ("SamGolden").GetComponent<Animator> ();
			spriteRender = transform.Find ("SamGolden").GetComponent<SpriteRenderer> ();
			speed *= 2;
		
	}

	private void ChangeToNormal()
	{
		
			transform.Find ("SamGolden").gameObject.SetActive (false);
			transform.Find ("SamBonzinho").gameObject.SetActive (true);
			anim = transform.Find ("SamBonzinho").GetComponent<Animator> ();
			spriteRender = transform.Find ("SamBonzinho").GetComponent<SpriteRenderer> ();
			speed /= 2;

	}

	void Fire(){
		if (Input.GetButtonDown ("Fire1")) 
		{
			if (hasPower) {
				if (onRight) {
					GameObject aux = Instantiate (FireBall, new Vector2 (transform.position.x + 0.45f, transform.position.y), Quaternion.identity);
					aux.GetComponent<FireBall> ().direction = new Vector2 (1f, -1f);
				} else {
					GameObject aux = Instantiate (FireBall, new Vector2 (transform.position.x - 0.45f, transform.position.y), Quaternion.identity);
					aux.GetComponent<FireBall> ().direction = new Vector2 (-1f, -1f); 
				}
				hasPower = false;
			}
		}
	}

	void OnCollisionEnter2D(Collision2D other){
		if(other.gameObject.CompareTag("FireBall")){
			hasPower = true;
			Destroy (other.gameObject);
		}

		else if (other.gameObject.CompareTag ("Ground")) {
			onGround = true;
			anim.SetBool("Jump",false);
		}

		else if(other.gameObject.CompareTag("Enemy"))
		{
			print ("Player: Enemy normal is: " + other.contacts [0].normal.y);
			if (other.contacts [0].normal.y <= 0) {
				isDead = true;
				canMove = false;
				anim.SetTrigger ("Die");
			}
		}

		else if(other.gameObject.CompareTag("BulletEnemy"))
		{
			isDead = true;
			canMove = false;
			anim.SetTrigger ("Die");
		}

		else if(other.gameObject.CompareTag("Hole"))
		{
			isDead = true;
			canMove = false;
			anim.SetTrigger ("Die");
		}


	}

	void OnCollisionExit2D(Collision2D other){
		if (other.gameObject.CompareTag ("Ground")) {
			onGround = false;
		}
	}
}
