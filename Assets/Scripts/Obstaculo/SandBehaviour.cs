﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SandBehaviour : MonoBehaviour {

	public float speedlowPlayer;
	public float jumplowPlayer;

	private PlayerController player;
	private Collider2D myCol;
	private bool isSink;
	// Use this for initialization
	void Start () {
		myCol = this.GetComponent<Collider2D> ();
		player = GameManager.Instance.player.GetComponent<PlayerController> ();
	}
	
	// Update is called once per frame
	void Update () {
		Sink ();
		Normal ();
	}

	void OnCollisionEnter2D(Collision2D other){
		if (other.gameObject.CompareTag ("Player")) {
			isSink = true;
			player.speed /= speedlowPlayer;
			player.jumpStrength /= jumplowPlayer;

		}
	}

	void OnCollisionStay2D(Collision2D other){
		if (other.gameObject.CompareTag ("Player")) {
			isSink = true;

		}
	}

	void OnCollisionExit2D(Collision2D other){
		if (other.gameObject.CompareTag ("Player")) {
			isSink = false;
			player.speed *= speedlowPlayer;
			player.jumpStrength *= jumplowPlayer;

		}
	}


	private void Sink()
	{
		if (isSink) {
			myCol.offset -= new Vector2 (0f, 0.01f);

			if (myCol.offset.y < -0.04) {
				print ("morre");
				player.isDead = true;
				player.canMove = false;
			}
		}
	}

	private void Normal()
	{
		if (!isSink && (myCol.offset.y < 0.07)) 
		{
			myCol.offset += new Vector2 (0f, 0.01f);
		}
	}
}
