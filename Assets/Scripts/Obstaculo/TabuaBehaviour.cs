﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabuaBehaviour : MonoBehaviour {

	public float timeToFall;
	public float timeToBack;

	private Collider2D myCol;
	private Animator anim;
	// Use this for initialization
	void Awake () {
		myCol = this.GetComponent<Collider2D> ();
		anim = this.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D other){
		if (other.gameObject.CompareTag ("Player")) {
			
			StartCoroutine (Cair ());

		}
	}

	IEnumerator Cair()
	{
		yield return new WaitForSeconds (timeToFall);
		myCol.isTrigger = true;
		anim.SetTrigger ("Caindo");
		yield return new WaitForSeconds (timeToBack);
		anim.SetTrigger ("Voltando");
		myCol.isTrigger = false;
	}
}
