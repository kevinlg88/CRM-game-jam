﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAnimation : MonoBehaviour {

	// Use this for initialization
	private Boss bossScript;
	private Animator anim;

	void Awake () 
	{
		bossScript = this.GetComponent<Boss> ();
		anim = this.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		InvestAnim ();
		DeadAnim ();
	}

	private void InvestAnim()
	{
		if (bossScript.isInvest) {
			anim.SetBool ("isDash", true);
		} 
		else {
			anim.SetBool ("isDash", false);
		}
		
	}

	private void DeadAnim ()
	{
		if (bossScript.isDead) {
			anim.SetTrigger ("isDead");
		}
	}
}
