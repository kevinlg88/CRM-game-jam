﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour {
	[Header("Power Probability (only stage 3)")]
	public List<float> powersProbs = new List<float>();

	[Header("Boss Attributes")]
	public float WaitBetweenAttacksStage1;
	public float WaitBetweenAttacksStage2;
	public float WaitBetweenAttacksStage3;

	private Transform playerTransf;
	private PlayerController playerScript;
	[HideInInspector]
	public bool playerisDead = false;
	private int life = 10;

	[Header("Invest Attributes")]
	public float investStrength;
	public float investDuration;
	private bool directionRight = false;
	private bool canSide = true;

	[Header("FireBall Attributes")]
	public GameObject fireball;

	//Boss States
	[HideInInspector]
	public bool onRight = false;
	[HideInInspector]
	public bool isInvest = false;
	[HideInInspector]
	public bool canAttack = true;
	[HideInInspector]
	public bool isDead = false;
	[HideInInspector]
	public bool isThrow = false;
	

	//Boss Stage
	private int bossStage = 1;

	private Rigidbody2D rb;
	void Awake()
	{
		rb = this.GetComponent<Rigidbody2D> ();
		playerScript = GameManager.Instance.player.GetComponent<PlayerController> ();
	}

	void Start () 
	{
		
	}

	void Update ()
	{
		//--- Boss Atributes ---
		BossSide ();
		ChangeBossStage (life);
		//-----------------------

		//--- Function Coroutines Controller ---
		ExecuteCorountine(bossStage);
		Invest ();
		VerifyPlayerDeath ();
		//---------------------------------------

	}

	//Quando Ocorre uma collisão
	void OnCollisionEnter2D(Collision2D other){

		if (other.collider.CompareTag("Enemy"))
		{
			DestroyObject (other.gameObject);
		}

		if(other.gameObject.CompareTag("Player"))
		{
			print ("Boss: PlayerNormal is " + other.contacts [0].normal.y);
			if (other.contacts [0].normal.y < 0) 
			{
				other.gameObject.GetComponent<Rigidbody2D> ().AddForce (Vector2.up * 200);
				life -= 1;
				print (""+life);
				if (life == 0) 
				{
					isDead = true;
					canAttack = false;
					isInvest = false;
					isThrow = false;
					StopAllCoroutines ();
					rb.velocity = new Vector2 (0, 0);
				}
			}
		}
	}


	//############################# Boss Function ##########################
	public void BossSide() // Called on update
	{
		if (canSide) 
		{
			playerTransf = GameManager.Instance.player.transform;
			if (this.transform.position.x > playerTransf.position.x) 
			{
				onRight = false;
				Vector3 theScale = transform.localScale;
				if (theScale.x > 0) 
				{
					theScale.x *= -1;

					transform.localScale = theScale;
				}
			} 

			else 
			{
				onRight = true;

				Vector3 theScale = transform.localScale;
				if (theScale.x < 0) 
				{
					theScale.x *= -1;

					transform.localScale = theScale;
				}
			}
		}
	}


	private void VerifyPlayerDeath()
	{
		if (playerScript.isDead) {
			playerisDead = true;
			canAttack = false;
			isInvest = false;
			isThrow = false;
			StopAllCoroutines ();
			rb.velocity = new Vector2 (0, 0);
		}
	}

	//############################## Boss Stage ############################
	private void ChangeBossStage(int hp) // Called on update
	{
		switch (hp) 
		{
		case 10:
			bossStage = 1;
			break;

		case 7:
			bossStage = 2;
			break;

		case 3:
			bossStage = 3;
			break;
		}
	}
	private void ExecuteCorountine(int stage) //Called on Update
	{
		if (canAttack) 
		{
			if (bossStage == 1) {
				StartCoroutine (Stage1 ());
			} else if (bossStage == 2) {
				StartCoroutine (Stage2 ());
			} else if (bossStage == 3) {
				StartCoroutine (Stage3 ());
			}
		}
	}

	//############################## Boss Stage corountines ############################
	IEnumerator Stage1() //Called on execute coroutines
	{
		canAttack = false;
		StartCoroutine (InvestStart ());
		yield return new WaitUntil(() => isInvest == false);
		yield return new WaitForSeconds (WaitBetweenAttacksStage1);
		canAttack = true;

		
	}

	IEnumerator Stage2() //Called on execute coroutines
	{
		canAttack = false;
		Fire ();
		yield return new WaitForSeconds (WaitBetweenAttacksStage2);
		canAttack = true;
		
	}

	IEnumerator Stage3() //Called on execute coroutines
	{
		canAttack = false;
		StartCoroutine (InvestStart ());
		yield return new WaitUntil(() => isInvest == false);
		Fire ();
		yield return new WaitForSeconds (WaitBetweenAttacksStage3);
		canAttack = true;
		

	}

	//############################## Throw FireBall ########################
	private void Fire() //Called on coroutines Stage2 and Stage3
	{
		isThrow = true;
		if (onRight) {
			GameObject aux = Instantiate (fireball, new Vector2 (transform.position.x + 0.45f, transform.position.y), Quaternion.identity);
			aux.GetComponent<FireBall> ().direction = new Vector2 (1f, -1f);
		} else {
			GameObject aux = Instantiate (fireball, new Vector2 (transform.position.x - 0.45f, transform.position.y), Quaternion.identity);
			aux.GetComponent<FireBall> ().direction = new Vector2 (-1f, -1f); 
		}
		isThrow = false;
	}
			

	//######################### Invest Functions ###########################
	IEnumerator InvestStart() //Called on coroutines Stage1 and Stage3
	{
		if (onRight) {
			directionRight = true;
		} else {
			directionRight = false;
		}
		canSide = false;
		isInvest = true;
		yield return new WaitForSeconds (investDuration);
		isInvest = false;
		rb.velocity = new Vector2 (0, 0);
		canSide = true;

	}
		
	private void Invest() //Called on update
	{
		if (isInvest) 
		{
			if (!directionRight) 
			{
				rb.velocity = Vector2.left * investStrength * Time.fixedDeltaTime;
			} 

			else if (directionRight) 
			{
				rb.velocity = Vector2.right * investStrength * Time.fixedDeltaTime;
			}

		}
	}


	//######################## Until Functions #############################
	private int RandomIndex (float[] probs) 
	{

		float total = 0;

		foreach (float elem in probs) 
		{
			total += elem;
		}

		float randomPoint = Random.value * total;

		for (int i= 0; i < probs.Length; i++) 
		{
			if (randomPoint < probs[i]) 
			{
				return i;
			}
			else 
			{
				randomPoint -= probs[i];
			}
		}
		return probs.Length - 1;
	}
}
