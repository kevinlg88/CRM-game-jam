﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public Transform player;
	public float offsetMaxX;
	public float offsetMinX;

	// Use this for initialization
	void Start () {
		player.GetComponent<PlayerController> ().offsetMinX = transform.position.x - offsetMinX;
		
	}
	
	// Update is called once per frame
	void Update () {
		CamMoviment ();
	}

	void CamMoviment(){
		if (player) {
			if (player.position.x > transform.position.x + offsetMaxX) {
				player.GetComponent<PlayerController> ().offsetMinX = transform.position.x - offsetMinX;
				transform.position = new Vector3(Mathf.Lerp(this.transform.position.x, player.position.x, 0.03f), transform.position.y, transform.position.z);
			}
		}
	}
}
