﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour {

	private Rigidbody2D rb2d;

	[System.NonSerialized] //Do not show in Inspector
	public Vector2 direction;

	public float speed;
	public bool staticc;

	void Awake(){
		rb2d = GetComponent<Rigidbody2D> ();
		if (!staticc) {
			direction = new Vector2 (1, -1);
		}
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		FireMovimento ();
	}

	void OnCollisionEnter2D(Collision2D other){

		if (!staticc) {
			Vector2 aux = other.contacts [0].normal;
		
			if (aux.x != 0) {
				direction = new Vector2 (-direction.x, direction.y);
			}
			if (aux.y == 1 || aux.y == -1) {
				direction = new Vector2 (direction.x, -direction.y);
			}
		}
	}

	void FireMovimento(){
		rb2d.velocity = direction * speed;
	}
}
